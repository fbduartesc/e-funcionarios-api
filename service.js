const database = require("./database");

exports.getItemsByStatus = function (status) {
	return database.execute("select * from funcionario.funcionarios where status = $1", [status]);
};

exports.updateItem = function(item){
	return database.execute("update funcionario.funcionarios set situacao = $1, update_on = $2 where codigo = $3", [item.situacao, item.updateOn, item.codigo]);
};

exports.getItems = function(){
	return database.execute("select * from namegator.item");
};

exports.saveItem = function(item){
	return database.execute("insert into namegator.item (type, description) values($1,$2) returning *", [item.type, item.description]);
};

exports.deleteItem = function(id){
	return database.execute("delete from namegator.item where id = $1", [id]);
};