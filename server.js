const {ApolloServer} = require("apollo-server");
const service = require("./service");

const typeDefs = `
	type Item{
		codigo: Int
		status: String
		nome: String
		nome_completo: String
		cargo: String
	}
	
	type Query{
		items(status: String): [Item]
	}
`;

const resolvers = {
    Query: {
        async items(_, args) {
            //return items.filter(item => item.status === args.status);
            const items = await service.getItemsByStatus(args.status);
            return items;
        }
    }
};

const server = new ApolloServer({typeDefs, resolvers,introspection : true,playground: true});
server.listen({ port: process.env.PORT || 4001 }).then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`);
});